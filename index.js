const express = require('express');
const app = express();
const cors = require('cors')
app.use(cors())
app.use(express.static('static'))
app.use(express.urlencoded({extended: false}))
var port = process.env.PORT || 8080;
const request = require('request')
//app.listen(port, () =>
//     console.log(`HTTP server with Express.js listening on the port: ${port}`)
// )
const http = require('http').Server(app).listen(port)
app.get('/', (req,res) => {
    res.sendFile(__dirname + '/static/chatclient/chatclient.html');
})
app.get('/echo.php', function(req,res){
    var data = req.body['data']
    res.send(data)
})
const MongoClient = require('mongodb').MongoClient;
const mongourl = "mongodb+srv://dasguptas2:j5mfV%23A81Q@cca-dasguptas2.lcynr.mongodb.net/cca-labs?retryWrites=true&w=majority";
const dbClient= new  MongoClient(mongourl,{useNewUrlParser: true, useUnifiedTopology: true});
dbClient.connect(err =>{if (err) throw err; console.log("Connected to the MongoDB cluster")})

const io = require('socket.io')(http);
console.log("socket.io server is running on port " + port);
app.get("/chat", (req,res)=>{
res.sendFile(__dirname + '/static/chatclient/index.html')
})
io.on('connection', (socketclient) => {
    console.log('A new client is connected!');
    socketclienthandler(socketclient)
})

function socketclienthandler(socketclient){
    console.log("Hello");
    var onlineClients = Object.keys(io.sockets.sockets).length;
    var welcomemessage = `${socketclient.id} is connected! Number of connected clients: ${onlineClients}`;
    console.log(welcomemessage);
    io.emit("online", welcomemessage);

    socketclient.on("registration",(username,password,email) => {
        request.post({
            url:"https://account-microservice.azurewebsites.net/registration",
            body:{username:username,password:password,email:email},
            json:true
        },
            function (error, response, body) {
                
                if (!error && response.statusCode == 200) {
                    if(body.error){
                        socketclient.emit("signUpFailed",body.error);
                    }else{
                        socketclient.emit("userSignUpSuccess",body.data);
                    }
                }
            }
        );

        // socket.broadcast.emit('register', username , password,email);
    });
    socketclient.on("login",(username,password) => {
        request.post({
            url:"https://account-microservice.azurewebsites.net/signin",
            body:{username:username,password:password},
            json:true
        },
            function (error, response, body) {
                
                if (!error && response.statusCode == 200) {
                    if(body.error){
                        socketclient.emit("loginFailed",body.error);
                    }else{
                        socketclient.authenticated =true;
                        socketclient.username=username;
                        body.data["id"]=socketclient.id;
                        socketclient.account=body.data;

                        socketclient.emit("loginSuccess",body.data);
                        io.emit("welcomemessage", `${socketclient.username} has joined the chat system`);                        
                    }
                }
            }
        );

        // socket.broadcast.emit('register', username , password,email);
    });

    socketclient.on("message", (data)=> {
        console.log("Data from client:" + data);
        console.log("socketclient.account:" + socketclient.account);
        console.log("socketclient.username:" + socketclient.username);
        console.log("socketclient:" + socketclient);
        io.emit("message", `${socketclient.username} says: ${data}`);
        
    });
    
    socketclient.on("typing", () => {
        console.log(`${socketclient.username} is typing ...`);
        // if(socketclient.authenticated==true){        
        socketclient.broadcast.emit("typing", `${socketclient.username} is typing ...`);
        // }
    });
  
    socketclient.on("disconnect", () => {
        var onlineClients = Object.keys(io.sockets.sockets).length;
        var byemessage = `${socketclient.id} is disconnected! Number of connected clients: ${onlineClients}`;
        console.log(byemessage);
        io.emit("online", byemessage);
    });
    
}

