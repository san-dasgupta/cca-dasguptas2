var server = createServer(httphandler);
var port = process.env.PORT || 8080;
server.listen(port);
console.log("httpserver is running at port: " + port);
import { readFile } from "fs";
import { parse } from 'url';
import { parse as _parse, stringify } from 'querystring';
function httphandler(request,response) {
    console.log("Get an HTTP Request: " + request.url);
    var fullpath = parse(request.url, true);
    if(fullpath.pathname ==="/echo.php") {
        if(request.method == "GET") {
            var query = _parse(stringify(fullpath.query));
            response.writeHead(200, {'Content-Type': 'text/html'});
            response.write(query.data);
            return response.end();
        }
        else if(request.method == "POST") {
            let postdata= '';
            request.on('data', (chunk) => {
                postdata += chunk;
        });
        request.on('end', () => {
            postdata = _parse(postdata);
            response.writeHead(200, {'Content-Type': 'text/html'});
            response.write(postdata.data);
            return response.end();
        });
    }
    return;
}
    var localfile = ".." + fullpath.pathname;
    console.log("Debug: Server's local file: " + localfile);
    readFile(localfile, (error, filecontent) => 
    {
        if (error) {
            response.writeHead(404);
            return response.end(fullpath.pathname + " is not found on this server. ");
        }
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write(filecontent);
        return response.end();
    });
    
}